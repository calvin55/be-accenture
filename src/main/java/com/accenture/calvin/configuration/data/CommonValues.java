/*
 * [Copyright (c) by Calvin Mampioper as CM25,Jakarta]
 * [Telp  : 081353000852]
 * [Email : calvin25.mampioper@gmail.com || calvin.mampioper@iconpln.co.id]
 * -----------------------------------------------------------------------------------------------------
 * CM25 alias calvin Create : CommonValues.java
 * Module : be-accenture
 */

package com.accenture.calvin.configuration.data;

public class CommonValues {

    public static final String HEADER_AUTHORIZATION = "Authorization";
    public static final String HEADER_APP_DATA = "App-Data";

    public static final String VERSION_API = "api/v1/";

}