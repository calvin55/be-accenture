/*
 * [Copyright (c) by Calvin Mampioper as CM25,Jakarta]
 * [Telp  : 081353000852]
 * [Email : calvin25.mampioper@gmail.com || calvin.mampioper@iconpln.co.id]
 * -----------------------------------------------------------------------------------------------------
 * CM25 alias calvin Create : PaginationDto.java
 * Module : be-accenture
 */

package com.accenture.calvin.configuration;

import lombok.Data;

@Data
public class PaginationDto {

    private Object content;
    private Integer totalElements;
    private Integer size;
    private Integer number;

}