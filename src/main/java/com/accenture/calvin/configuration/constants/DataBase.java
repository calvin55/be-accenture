/*
 * [Copyright (c) by Calvin Mampioper as CM25,Jakarta]
 * [Telp  : 081353000852]
 * [Email : calvin25.mampioper@gmail.com || calvin.mampioper@iconpln.co.id]
 * -----------------------------------------------------------------------------------------------------
 * CM25 alias calvin Create : DataBase.java
 * Module : be-accenture
 */

package com.accenture.calvin.configuration.constants;

public interface DataBase {
    String TABLE_USER = "user";
    String TABLE_USER_SETTING = "setting";
    String TABLE_USER_KEY_LOV = "key_lov";
    String ID_USER_SETTING = "id";
    String ID_USER = "id";
}