/*
 * [Copyright (c) by Calvin Mampioper as CM25,Jakarta]
 * [Telp  : 081353000852]
 * [Email : calvin25.mampioper@gmail.com || calvin.mampioper@iconpln.co.id]
 * -----------------------------------------------------------------------------------------------------
 * CM25 alias calvin Create : MessageCrud.java
 * Module : be-accenture
 */

package com.accenture.calvin.configuration.constants;

public interface MessageCrud {
    String PREFIX_VERSION_API = "v1/";
    String DATA_SAVE = "Data Saved Successfully";
    String DATA_FAILED = "Data Failed to Save";
    String DATA_UPDATED = "Data Successfully Updated";
    String DATA_DELETED = "Data Deleted Successfully";
    String DATA_NOT_FOUND = "Data Not Found";
    String DATA_EXISTS = "Data Already Exists";
    String DATA_LIST = "Get Data Success";
}