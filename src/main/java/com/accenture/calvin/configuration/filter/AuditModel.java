/*
 * [Copyright (c) by Calvin Mampioper as CM25,Jakarta]
 * [Telp  : 081353000852]
 * [Email : calvin25.mampioper@gmail.com || calvin.mampioper@iconpln.co.id]
 * -----------------------------------------------------------------------------------------------------
 * CM25 alias calvin Create : AuditModel.java
 * Module : be-accenture
 */

package com.accenture.calvin.configuration.filter;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.io.Serializable;
import java.util.Date;

@Data
@MappedSuperclass
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
public class AuditModel implements Serializable {

    private static final long serialVersionUID = 5903087993747467141L;

    @CreatedBy
    @Column(name = "user_created", updatable = false)
    private String userCreated;

    @CreationTimestamp
    @Column(name = "time_created", updatable = false)
    private Date timeCreated;

    @LastModifiedBy
    @Column(name = "user_update")
    private String userUpdate;

    @UpdateTimestamp
    @Column(name = "last_update")
    private Date lastUpdate;

    @PrePersist
    public void prePersist() {
        lastUpdate = timeCreated = new Date();
    }

    @PreUpdate
    public void preUpdate() {
        lastUpdate = new Date();
    }

}