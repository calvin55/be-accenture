/*
 * [Copyright (c) by Calvin Mampioper as CM25,Jakarta]
 * [Telp  : 081353000852]
 * [Email : calvin25.mampioper@gmail.com || calvin.mampioper@iconpln.co.id]
 * -----------------------------------------------------------------------------------------------------
 * CM25 alias calvin Create : AuditConfig.java
 * Module : be-accenture
 */

package com.accenture.calvin.configuration.filter;

import com.accenture.calvin.configuration.data.CommonValues;
import com.accenture.calvin.configuration.data.UserData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.Optional;

@Slf4j
@Configuration
@EnableJpaAuditing
public class AuditConfig {

    @Bean
    public AuditorAware<String> auditorProvider() {
        return () -> {
            String auditor = "System";
            ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
            if (attr != null) {
                UserData data = (UserData) attr.getRequest().getAttribute(CommonValues.HEADER_APP_DATA);
                if (data != null) auditor = data.getEmail();
            }
            log.info("Auditor : {}", auditor);
            return Optional.of(auditor);
        };
    }
}