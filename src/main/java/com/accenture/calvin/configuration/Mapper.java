/*
 * [Copyright (c) by Calvin Mampioper as CM25,Jakarta]
 * [Telp  : 081353000852]
 * [Email : calvin25.mampioper@gmail.com || calvin.mampioper@iconpln.co.id]
 * -----------------------------------------------------------------------------------------------------
 * CM25 alias calvin Create : Mapper.java
 * Module : be-accenture
 */

package com.accenture.calvin.configuration;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Mapper {
    @Bean
    public static ModelMapper modelMapperConfig() {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setAmbiguityIgnored(true);
        return modelMapper;
    }

}