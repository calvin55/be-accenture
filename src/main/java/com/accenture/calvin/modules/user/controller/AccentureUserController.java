/*
 * [Copyright (c) by Calvin Mampioper as CM25,Jakarta]
 * [Telp  : 081353000852]
 * [Email : calvin25.mampioper@gmail.com || calvin.mampioper@iconpln.co.id]
 * -----------------------------------------------------------------------------------------------------
 * CM25 alias calvin Create : AccentureUserController.java
 * Module : be-accenture
 */

package com.accenture.calvin.modules.user.controller;

import com.accenture.calvin.modules.user.dto.AccentureUserCreateDTO;
import com.accenture.calvin.modules.user.dto.AccentureUserUpdateDTO;
import com.accenture.calvin.modules.user.service.AccentureUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

import static com.accenture.calvin.configuration.constants.MessageCrud.PREFIX_VERSION_API;

@RestController
@RequestMapping(value = PREFIX_VERSION_API + "users", produces = MediaType.APPLICATION_JSON_VALUE)
public class AccentureUserController {
    @Autowired
    private AccentureUserService service;

    @PostMapping("save")
    public ResponseEntity<?> save(@RequestBody AccentureUserCreateDTO dto) {
        return new ResponseEntity<>(service.save(dto), HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<?> list(@RequestParam Optional<String> firstName, @RequestParam Optional<String> isActive, @RequestParam Optional<String> keyword, @RequestParam Optional<Integer> page, @RequestParam Optional<Integer> size, @RequestParam Optional<String> orderBy, @RequestParam Optional<String> order) {
        return new ResponseEntity<>(service.list(firstName, isActive, keyword, page, size, orderBy, order), HttpStatus.OK);
    }

    @GetMapping("findById/{id}")
    public ResponseEntity<?> findById(@PathVariable Integer id) {
        return new ResponseEntity<>(service.findById(id), HttpStatus.OK);
    }

    @PutMapping("update/{id}")
    public ResponseEntity<?> update(@PathVariable Integer id, @RequestBody AccentureUserUpdateDTO updateDto) {
        return new ResponseEntity<>(service.update(id, updateDto), HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable Integer id) {
        return new ResponseEntity<>(service.delete(id), HttpStatus.OK);
    }
}