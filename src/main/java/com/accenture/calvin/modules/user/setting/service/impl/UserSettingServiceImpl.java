/*
 * [Copyright (c) by Calvin Mampioper as CM25,Jakarta]
 * [Telp  : 081353000852]
 * [Email : calvin25.mampioper@gmail.com || calvin.mampioper@iconpln.co.id]
 * -----------------------------------------------------------------------------------------------------
 * CM25 alias calvin Create : UserSettingServiceImpl.java
 * Module : be-accenture
 */

package com.accenture.calvin.modules.user.setting.service.impl;

import com.accenture.calvin.configuration.constants.MessageCrud;
import com.accenture.calvin.modules.user.setting.dto.UserSettingCreateDTO;
import com.accenture.calvin.modules.user.setting.dto.UserSettingDetailDTO;
import com.accenture.calvin.modules.user.setting.dto.UserSettingUpdateDTO;
import com.accenture.calvin.modules.user.setting.entity.UserSetting;
import com.accenture.calvin.modules.user.setting.repository.UserSettingRepository;
import com.accenture.calvin.modules.user.setting.service.UserSettingService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Nonnull;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserSettingServiceImpl implements UserSettingService, MessageCrud {
    @Autowired
    private UserSettingRepository repository;
    private ModelMapper modelMapper = new ModelMapper();

    @Override
    public Object save(UserSettingCreateDTO dto) {
        Map<String, Object> map = new LinkedHashMap<>();
        UserSetting m = new UserSetting();
        m.setKeyx(dto.getKeyx());
        m.setValuex(dto.getValuex());
        repository.save(m);
        map.put("success", true);
        map.put("message", DATA_SAVE);
        map.put("data", mapToUserDetailDTO(m));
        return map;
    }

    @Override
    public Object update(@Nonnull Integer id, UserSettingUpdateDTO dto) {
        Map<String, Object> map = new LinkedHashMap<>();
        UserSetting m = resolveUserById(id);
        if (m != null) {
            m.setKeyx(dto.getKeyx());
            m.setValuex(dto.getValuex());
            map.put("success", true);
            map.put("message", DATA_UPDATED);
            map.put("data", mapToUserDetailsDtoPage(m));
            repository.save(m);
        } else {
            map.put("success", false);
            map.put("data", DATA_EXISTS);
        }
        return map;
    }

    @Override
    public Object delete(@Nonnull Integer id) {
        Map<String, Object> map = new LinkedHashMap<>();
        UserSetting m = resolveUserById(id);
        if (m != null) {
            repository.delete(m);
            map.put("success", true);
            map.put("message", DATA_DELETED);
        } else {
            map.put("success", false);
            map.put("message", DATA_NOT_FOUND);
        }
        return map;
    }

    private UserSettingDetailDTO mapToUserDetailDTO(UserSetting m) {
        UserSettingDetailDTO dto = modelMapper.map(m, UserSettingDetailDTO.class);
        return dto;
    }

    private UserSettingDetailDTO mapToUserDetailsDtoPage(UserSetting m) {
        return modelMapper.map(m, UserSettingDetailDTO.class);
    }

    private UserSetting resolveUserById(Integer id) {
        Optional<UserSetting> optional = repository.findById(id);
        return optional.orElse(null);
    }
}