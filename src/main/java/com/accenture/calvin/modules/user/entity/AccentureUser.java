/*
 * [Copyright (c) by Calvin Mampioper as CM25,Jakarta]
 * [Telp  : 081353000852]
 * [Email : calvin25.mampioper@gmail.com || calvin.mampioper@iconpln.co.id]
 * -----------------------------------------------------------------------------------------------------
 * CM25 alias calvin Create : AccentureUser.java
 * Module : be-accenture
 */

package com.accenture.calvin.modules.user.entity;

import com.accenture.calvin.configuration.constants.DataBase;
import com.accenture.calvin.configuration.filter.AuditModel;
import com.accenture.calvin.modules.user.setting.entity.UserSetting;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = DataBase.TABLE_USER)
public class AccentureUser extends AuditModel implements Serializable {
    private static final long serialVersionUID = 2939831935524541787L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Basic(optional = false)
    @Column(unique = true, length = 16)
    private String ssn;
    @Basic(optional = false)
    @Column(length = 100)
    private String firstName;
    @Column(length = 100)
    private String middleName;
    @Column(length = 100)
    private String familyName;
    @Temporal(TemporalType.DATE)
    private Date birthDate;
    @Basic(optional = false)
    private Boolean isActive;
    @Temporal(TemporalType.TIMESTAMP)
    private Date deletedTime;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_user", referencedColumnName = "id")
    private List<UserSetting> keyLovList = new ArrayList<>();
}