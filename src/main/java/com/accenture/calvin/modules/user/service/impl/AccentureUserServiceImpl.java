/*
 * [Copyright (c) by Calvin Mampioper as CM25,Jakarta]
 * [Telp  : 081353000852]
 * [Email : calvin25.mampioper@gmail.com || calvin.mampioper@iconpln.co.id]
 * -----------------------------------------------------------------------------------------------------
 * CM25 alias calvin Create : AccentureUserServiceImpl.java
 * Module : be-accenture
 */

package com.accenture.calvin.modules.user.service.impl;

import com.accenture.calvin.configuration.constants.DataBase;
import com.accenture.calvin.configuration.constants.MessageCrud;
import com.accenture.calvin.modules.user.dto.AccentureUserCreateDTO;
import com.accenture.calvin.modules.user.dto.AccentureUserDetailDTO;
import com.accenture.calvin.modules.user.dto.AccentureUserUpdateDTO;
import com.accenture.calvin.modules.user.entity.AccentureUser;
import com.accenture.calvin.modules.user.repository.AccentureUserRepository;
import com.accenture.calvin.modules.user.service.AccentureUserService;
import com.accenture.calvin.modules.user.setting.entity.UserSetting;
import lombok.RequiredArgsConstructor;
import org.apache.commons.text.CaseUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.annotation.Nonnull;
import java.util.*;
import java.util.function.Function;

@Service
@RequiredArgsConstructor
public class AccentureUserServiceImpl implements AccentureUserService, MessageCrud {
    @Autowired
    private AccentureUserRepository repository;
    private ModelMapper modelMapper = new ModelMapper();
    private String toStrJoiner;

    public static boolean isNumeric(String strNum) {
        if (strNum == null) {
            return false;
        }
        try {
            double d = Double.parseDouble(strNum);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    @Override
    public Object save(AccentureUserCreateDTO dto) {
        Map<String, Object> map = new LinkedHashMap<>();
        StringJoiner joiner = new StringJoiner(",");
        AccentureUser m = new AccentureUser();
        m.setSsn(dto.getSsn());
        m.setFirstName(dto.getFirstName());
        m.setMiddleName(dto.getMiddleName());
        m.setFamilyName(dto.getFamilyName());
        m.setBirthDate(dto.getBirthDate());
        m.setIsActive(true);
        //user setting
        UserSetting u = new UserSetting();
        u.setKeyx(dto.getKeyx());
        u.setValuex(dto.getValuex());
        if (isNumeric(u.getValuex())) {
            int num = Integer.parseInt(dto.getValuex()); // int number
            while (num > 0) {
                Integer p = num % 10;
                joiner.add(String.valueOf(p));
                toStrJoiner = joiner.toString();
                num = num / 10;
            }
        } else {
            joiner.add(u.getValuex());
            toStrJoiner = joiner.toString();
        }
        u.setValuex(toStrJoiner);
        m.getKeyLovList().add(u);

        repository.save(m);
        map.put("success", true);
        map.put("message", DATA_SAVE);
        map.put("data", mapToUserDetailDTO(m));
        return map;
    }

    @Override
    public Object update(@Nonnull Integer id, AccentureUserUpdateDTO dto) {
        Map<String, Object> map = new LinkedHashMap<>();
        AccentureUser m = resolveUserById(id);
        if (m != null) {
            m.setSsn(dto.getSsn());
            m.setFirstName(dto.getFirstName());
            m.setMiddleName(dto.getMiddleName());
            m.setFamilyName(dto.getFamilyName());
            m.setBirthDate(dto.getBirthDate());
            m.setIsActive(true);
            map.put("success", true);
            map.put("message", DATA_UPDATED);
            map.put("data", mapToUserDetailsDtoPage(m));
            repository.save(m);
        } else {
            map.put("success", false);
            map.put("data", DATA_EXISTS);
        }
        return map;
    }

    @Override
    public Object delete(@Nonnull Integer id) {
        Map<String, Object> map = new LinkedHashMap<>();
        AccentureUser m = resolveUserById(id);
        if (m != null) {
            m.setIsActive(false);
            TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
            m.setDeletedTime(new Date());
            repository.save(m);
            map.put("success", true);
            map.put("message", DATA_DELETED);
        } else {
            map.put("success", false);
            map.put("message", DATA_NOT_FOUND);
        }
        return map;
    }

    @Override
    public Object findById(@Nonnull Integer id) {
        Map<String, Object> map = new LinkedHashMap<>();
        AccentureUser m = resolveUserById(id);
        if (m != null) {
            map.put("success", true);
            map.put("message", DATA_LIST);
            map.put("data", mapToUserDetailsDtoPage(m));
        } else {
            map.put("success", false);
            map.put("data", DATA_NOT_FOUND);
        }
        return map;
    }

    @Override
    public Object list(Optional<String> firstNameOptional, Optional<String> isActiveOptional, Optional<String> keywordOptional, Optional<Integer> page, Optional<Integer> size, Optional<String> orderByOptional, Optional<String> orderOptional) {
        Map<String, Object> map = new LinkedHashMap<>();
        int pageRequest = 0;
        if (page.isPresent()) pageRequest = page.get() - 1;
        int sizeRequest = Integer.MAX_VALUE;
        if (size.isPresent()) sizeRequest = size.get();
        String keyword = "";
        if (keywordOptional.isPresent()) {
            keyword = keywordOptional.get().toLowerCase();
        }
        String firstName = keyword;
        if (firstNameOptional.isPresent()) {
            firstName = firstNameOptional.get().toLowerCase();
        }
        List<Boolean> isActiveIn = new ArrayList<>();
        if (isActiveOptional.isPresent()) {
            if (isActiveOptional.get().equals("true")) {
                isActiveIn.add(true);
            } else {
                isActiveIn.add(false);
            }
        } else {
            isActiveIn.add(true);
            isActiveIn.add(false);
        }
        String orderBy = DataBase.ID_USER;
        if (orderByOptional.isPresent()) {
            orderBy = orderByOptional.get().toLowerCase();
        }
        orderBy = CaseUtils.toCamelCase(orderBy, false, '_');
        String order = "desc";
        if (orderOptional.isPresent()) {
            order = orderOptional.get().toLowerCase();
        }
        Pageable pageable;
        if (order.equals("asc")) {
            pageable = PageRequest.of(pageRequest, sizeRequest, Sort.by(orderBy).ascending());
        } else {
            pageable = PageRequest.of(pageRequest, sizeRequest, Sort.by(orderBy).descending());
        }
        Page<AccentureUser> accentureUserPage;
        if (keywordOptional.isPresent()) {
            accentureUserPage = repository.findAllByFirstNameContainingIgnoreCaseAndIsActiveInAndDeletedTimeIsNull(firstName, isActiveIn, pageable);
        } else {
            accentureUserPage = repository.findAllByFirstNameContainingIgnoreCaseAndIsActiveInAndDeletedTimeIsNull(firstName, isActiveIn, pageable);
        }
        map.put("success", true);
        map.put("message", DATA_LIST);
        map.put("data", mapToUserDetailsPage(accentureUserPage));
        return map;
    }

    private AccentureUserDetailDTO mapToUserDetailDTO(AccentureUser m) {
        AccentureUserDetailDTO dto = modelMapper.map(m, AccentureUserDetailDTO.class);
        return dto;
    }

    private AccentureUserDetailDTO mapToUserDetailsDtoPage(AccentureUser m) {
        return modelMapper.map(m, AccentureUserDetailDTO.class);
    }

    private AccentureUser resolveUserById(Integer id) {
        Optional<AccentureUser> optional = repository.findById(id);
        return optional.orElse(null);
    }

    private Page<AccentureUserDetailDTO> mapToUserDetailsPage(Page<AccentureUser> m) {
        Page<AccentureUserDetailDTO> dtoPage = m.map(new Function<AccentureUser, AccentureUserDetailDTO>() {
            @Override
            public AccentureUserDetailDTO apply(AccentureUser m) {
                return mapToUserDetailsDtoPage(m);
            }
        });
        return dtoPage;
    }
}