/*
 * [Copyright (c) by Calvin Mampioper as CM25,Jakarta]
 * [Telp  : 081353000852]
 * [Email : calvin25.mampioper@gmail.com || calvin.mampioper@iconpln.co.id]
 * -----------------------------------------------------------------------------------------------------
 * CM25 alias calvin Create : AccentureUserService.java
 * Module : be-accenture
 */

package com.accenture.calvin.modules.user.service;

import com.accenture.calvin.modules.user.dto.AccentureUserCreateDTO;
import com.accenture.calvin.modules.user.dto.AccentureUserUpdateDTO;

import javax.annotation.Nonnull;
import java.util.Optional;

public interface AccentureUserService {
    Object save(AccentureUserCreateDTO dto);

    Object update(@Nonnull Integer id, AccentureUserUpdateDTO dto);

    Object delete(@Nonnull Integer id);

    Object findById(@Nonnull Integer id);

    Object list(Optional<String> firstNameOptional, Optional<String> isActiveOptional,
                Optional<String> keywordOptional, Optional<Integer> page,
                Optional<Integer> size, Optional<String> orderByOptional,
                Optional<String> orderOptional);
}