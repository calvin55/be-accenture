/*
 * [Copyright (c) by Calvin Mampioper as CM25,Jakarta]
 * [Telp  : 081353000852]
 * [Email : calvin25.mampioper@gmail.com || calvin.mampioper@iconpln.co.id]
 * -----------------------------------------------------------------------------------------------------
 * CM25 alias calvin Create : UserKeyLovService.java
 * Module : be-accenture
 */

package com.accenture.calvin.modules.user.setting.key_lov.service;

import com.accenture.calvin.modules.user.setting.key_lov.dto.UserKeyLovCreateDTO;
import com.accenture.calvin.modules.user.setting.key_lov.dto.UserKeyLovDetailDTO;
import com.accenture.calvin.modules.user.setting.key_lov.dto.UserKeyLovUpdateDTO;

public interface UserKeyLovService {
    Object save(UserKeyLovCreateDTO dto);

    Object update(Long id, UserKeyLovUpdateDTO dto);

    Object detail(UserKeyLovDetailDTO dto);
}