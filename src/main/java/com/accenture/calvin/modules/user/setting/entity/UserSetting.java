/*
 * [Copyright (c) by Calvin Mampioper as CM25,Jakarta]
 * [Telp  : 081353000852]
 * [Email : calvin25.mampioper@gmail.com || calvin.mampioper@iconpln.co.id]
 * -----------------------------------------------------------------------------------------------------
 * CM25 alias calvin Create : UserSetting.java
 * Module : be-accenture
 */

package com.accenture.calvin.modules.user.setting.entity;

import com.accenture.calvin.configuration.constants.DataBase;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = DataBase.TABLE_USER_SETTING)
public class UserSetting implements Serializable {
    private static final long serialVersionUID = 2939831935524541787L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Basic(optional = false)
    @Column(length = 100)
    private String keyx;
    @Basic(optional = false)
    @Column(length = 100)
    private String valuex;
}