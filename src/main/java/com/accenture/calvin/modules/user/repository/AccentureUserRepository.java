/*
 * [Copyright (c) by Calvin Mampioper as CM25,Jakarta]
 * [Telp  : 081353000852]
 * [Email : calvin25.mampioper@gmail.com || calvin.mampioper@iconpln.co.id]
 * -----------------------------------------------------------------------------------------------------
 * CM25 alias calvin Create : AccentureUserRepository.java
 * Module : be-accenture
 */

package com.accenture.calvin.modules.user.repository;

import com.accenture.calvin.modules.user.entity.AccentureUser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccentureUserRepository extends JpaRepository<AccentureUser, Integer> {
    Page<AccentureUser> findAllByFirstNameContainingIgnoreCaseAndIsActiveInAndDeletedTimeIsNull(
            String firstName, List<Boolean> isActive, Pageable pageable
    );
}