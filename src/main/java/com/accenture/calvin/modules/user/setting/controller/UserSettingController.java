/*
 * [Copyright (c) by Calvin Mampioper as CM25,Jakarta]
 * [Telp  : 081353000852]
 * [Email : calvin25.mampioper@gmail.com || calvin.mampioper@iconpln.co.id]
 * -----------------------------------------------------------------------------------------------------
 * CM25 alias calvin Create : UserSettingController.java
 * Module : be-accenture
 */

package com.accenture.calvin.modules.user.setting.controller;

import com.accenture.calvin.modules.user.setting.dto.UserSettingCreateDTO;
import com.accenture.calvin.modules.user.setting.dto.UserSettingUpdateDTO;
import com.accenture.calvin.modules.user.setting.service.UserSettingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static com.accenture.calvin.configuration.constants.MessageCrud.PREFIX_VERSION_API;

@RestController
@RequestMapping(value = PREFIX_VERSION_API + "user-setting", produces = MediaType.APPLICATION_JSON_VALUE)
public class UserSettingController {
    @Autowired
    private UserSettingService service;

    @PostMapping("save")
    public ResponseEntity<?> save(@RequestBody UserSettingCreateDTO dto) {
        return new ResponseEntity<>(service.save(dto), HttpStatus.OK);
    }

    @PutMapping("update/{id}")
    public ResponseEntity<?> update(@PathVariable Integer id, @RequestBody UserSettingUpdateDTO updateDto) {
        return new ResponseEntity<>(service.update(id, updateDto), HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable Integer id) {
        return new ResponseEntity<>(service.delete(id), HttpStatus.OK);
    }
}