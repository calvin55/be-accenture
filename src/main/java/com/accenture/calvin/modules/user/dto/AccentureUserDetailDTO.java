/*
 * [Copyright (c) by Calvin Mampioper as CM25,Jakarta]
 * [Telp  : 081353000852]
 * [Email : calvin25.mampioper@gmail.com || calvin.mampioper@iconpln.co.id]
 * -----------------------------------------------------------------------------------------------------
 * CM25 alias calvin Create : AccentureUserDetailDTO.java
 * Module : be-accenture
 */

package com.accenture.calvin.modules.user.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

import java.util.Date;

@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class AccentureUserDetailDTO {
    private Integer id;
    private String ssn;
    private String firstName;
    private String middleName;
    private String familyName;
    private Boolean isActive;
    private String userCreated;
    private Date birthDate;
    private Date deletedTime;
}