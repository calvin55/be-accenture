/*
 * [Copyright (c) by Calvin Mampioper as CM25,Jakarta]
 * [Telp  : 081353000852]
 * [Email : calvin25.mampioper@gmail.com || calvin.mampioper@iconpln.co.id]
 * -----------------------------------------------------------------------------------------------------
 * CM25 alias calvin Create : UserSettingService.java
 * Module : be-accenture
 */

package com.accenture.calvin.modules.user.setting.service;

import com.accenture.calvin.modules.user.setting.dto.UserSettingCreateDTO;
import com.accenture.calvin.modules.user.setting.dto.UserSettingUpdateDTO;

import javax.annotation.Nonnull;

public interface UserSettingService {
    Object save(UserSettingCreateDTO dto);

    Object update(@Nonnull Integer id, UserSettingUpdateDTO dto);

    Object delete(@Nonnull Integer id);
}