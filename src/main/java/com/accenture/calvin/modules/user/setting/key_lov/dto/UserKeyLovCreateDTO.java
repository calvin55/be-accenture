/*
 * [Copyright (c) by Calvin Mampioper as CM25,Jakarta]
 * [Telp  : 081353000852]
 * [Email : calvin25.mampioper@gmail.com || calvin.mampioper@iconpln.co.id]
 * -----------------------------------------------------------------------------------------------------
 * CM25 alias calvin Create : UserKeyLovCreateDTO.java
 * Module : be-accenture
 */

package com.accenture.calvin.modules.user.setting.key_lov.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class UserKeyLovCreateDTO {
    public String biometricLogin;
    public String pushNotification;
    public String smsNotification;
    public String showOnboard;
    public String widgetOrder;
}